﻿using System.Data;
using System.Data.SqlClient;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using OrmComparisons.NHibernate.Models;

namespace OrmComparisons.NHibernate;

public class Helper
{
    public static ISessionFactory Init()
    {
        
        string connectionString = "Server=localhost,1433;User=sa;Password=pass@word1";
        using var conn = new SqlConnection(connectionString);
        var cmd = conn.CreateCommand();
        cmd.CommandText = @"IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'Bachelor')
                                BEGIN
                                    CREATE DATABASE [Bachelor]
                                END";

        if (conn.State != ConnectionState.Open)
            conn.Open();

        cmd.ExecuteNonQuery();

        var config = new Configuration().Configure().AddAssembly(typeof(Animal).Assembly);
        var schema = new SchemaExport(config);
        schema.Create(true, true);
        return config.BuildSessionFactory();
    }
}