﻿using NHibernate;
using OrmComparisons.NHibernate.Models;

namespace OrmComparisons.NHibernate.Samples;

/// <summary>
/// see Mappings.vehicle.hbm.xml
/// </summary>
public class TpcImplicitPolymorphismSample
{
    public static void Run(ISessionFactory sessionFactory)
    {
        InsertData(sessionFactory);

        using var sess = sessionFactory.OpenSession();
        foreach (var vehicle in sess.Query<Vehicle>().ToList())
            Console.WriteLine($"{vehicle.OwnerName} owns a / an {vehicle.GetType().Name}");
    }

    private static void InsertData(ISessionFactory sessionFactory)
    {
        var items = new List<Vehicle>
        {
            new Car("Jack", "VW", false),
            new Car("Jina", "Tesla", true),
            new Car("Jane", "BMW", false),
            new Airplane("Bill Gates", 20, 30),
            new Airplane("Zuckerberg", 30, 35),
            new Airplane("Kevin", 15, 25)
        };

        using var sess = sessionFactory.OpenSession();

        items.ForEach(item => sess.Save(item));
    }
}