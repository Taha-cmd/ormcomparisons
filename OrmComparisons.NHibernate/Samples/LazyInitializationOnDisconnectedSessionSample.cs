﻿using NHibernate;
using OrmComparisons.NHibernate.Models;

namespace OrmComparisons.NHibernate.Samples;

public static class LazyInitializationOnDisconnectedSessionSample
{
    public static void Run(ISessionFactory sessionFactory)
    {
        using (var session = sessionFactory.OpenSession())
        {
            //in the first session, we save a person instance 
            //whose pet is a cat
            var entity = new Person
            {
                Pet = new Dog
                {
                    Weight = 10,
                    LovesWalking = false
                }
            };

            session.Save(entity);
        }

        Person person = null;
        using (var session = sessionFactory.OpenSession())
        {
            person = session.Query<Person>().FirstOrDefault();
        }

        try
        {
            Console.WriteLine(person.Pet.Weight);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}