﻿using NHibernate;
using OrmComparisons.NHibernate.Models;

namespace OrmComparisons.NHibernate.Samples;

public static class WrongProxyTypeSample
{
    public static void Run(ISessionFactory sessionFactory)
    {
        using (var session = sessionFactory.OpenSession())
        {
            //in the first session, we save a person instance 
            //whose pet is a cat
            var entity = new Person
            {
                Pet = new Cat
                {
                    Weight = 5.5d,
                    LovesSleeping = true
                }
            };
            session.Save(entity);
        }

        using (var session = sessionFactory.OpenSession())
        {
            //in a new session, query the previously saved person
            //the Pet property, which is of Type Animal, will be initialized as an AnimalProxy, although the actual instance is a Cat
            //the concrete type cannot be known, hard casting the instance to the actual type will cause an exception
            var person = session.Query<Person>().FirstOrDefault();
            Console.WriteLine(person?.Pet.GetType());

            try
            {
                var cat = (Cat)person?.Pet;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}