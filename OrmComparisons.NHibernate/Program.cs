﻿using System.Data.SqlClient;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using OrmComparisons.NHibernate;
using OrmComparisons.NHibernate.Models;
using OrmComparisons.NHibernate.Samples;

var sessionFactory = Helper.Init();

TpcImplicitPolymorphismSample.Run(sessionFactory);
LazyInitializationOnDisconnectedSessionSample.Run(sessionFactory);
WrongProxyTypeSample.Run(sessionFactory);


Console.WriteLine();