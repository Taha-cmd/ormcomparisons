﻿namespace OrmComparisons.NHibernate.Models;

public class Airplane : Vehicle
{
    public Airplane()
    {
    }

    public Airplane(string ownerName, int capacity, double maxHeight)
    {
        OwnerName = ownerName;
        Capacity = capacity;
        MaxHeight = maxHeight;
    }

    public virtual int Capacity { get; set; }
    public virtual double MaxHeight { get; set; }
}