﻿namespace OrmComparisons.NHibernate.Models;

public class Car : Vehicle
{
    public Car()
    {
    }

    public Car(string ownerName, string manufacturer, bool isElectrical)
    {
        Manufacturer = manufacturer;
        IsElectrical = isElectrical;
        OwnerName = ownerName;
    }

    public virtual string Manufacturer { get; set; }
    public virtual bool IsElectrical { get; set; }
}