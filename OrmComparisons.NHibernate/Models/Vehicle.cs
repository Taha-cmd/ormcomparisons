﻿namespace OrmComparisons.NHibernate.Models;

public class Vehicle
{
    public virtual long Id { get; set; }
    public virtual string OwnerName { get; set; }
}