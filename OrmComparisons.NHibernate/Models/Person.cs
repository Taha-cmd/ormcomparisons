﻿namespace OrmComparisons.NHibernate.Models;

public class Person
{
    public virtual long Id { get; set; }
    public virtual Animal Pet { get; set; }
}