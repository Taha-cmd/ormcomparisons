﻿namespace OrmComparisons.NHibernate.Models;

public class Animal
{
    public virtual long Id { get; set; }
    public virtual double Weight { get; set; }
}