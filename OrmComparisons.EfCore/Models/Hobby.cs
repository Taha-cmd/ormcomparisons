﻿namespace OrmComparisons.EfCore.Models;

public class Hobby
{
    public int HobbyId { get; set; }
    public string Name { get; set; }

    public Hobby()
    {
        
    }

    public Hobby(string name)
    {
        this.Name = name;
    }
}