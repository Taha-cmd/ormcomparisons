﻿namespace OrmComparisons.EfCore.Models;

public class Person
{
    public Person()
    {
        
    }

    public Person(string name, ICollection<Hobby> hobbies)
    {
        Name = name;
        Hobbies = hobbies;
    }
    public int PersonId { get; set; }
    public string Name { get; set; }
    public virtual ICollection<Hobby> Hobbies { get; set; }
}