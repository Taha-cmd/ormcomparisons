﻿using OrmComparisons.EfCore;
using OrmComparisons.EfCore.Models;
using OrmComparisons.EfCore.Samples;

var context =  new ApplicationContext();

context.Database.EnsureDeleted();
context.Database.EnsureCreated();

LazyInitializationOnDisconnectedSessionSample.Run();
NplusOneProblemSample.Run();

Console.WriteLine();