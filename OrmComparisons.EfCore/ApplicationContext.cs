﻿using Microsoft.EntityFrameworkCore;
using OrmComparisons.EfCore.Models;

namespace OrmComparisons.EfCore;

public class ApplicationContext : DbContext
{
    public int StatementCounter { get; set; } = -1;
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        //https://stackoverflow.com/questions/4867602/entity-framework-there-is-already-an-open-datareader-associated-with-this-comma
        //MultipleActiveResultSets=true is needed to demonstrate the N+1 problem
        string connectionString = "Server=localhost,1433;Database=Bachelor2;User=sa;Password=pass@word1;MultipleActiveResultSets=true";
        optionsBuilder.UseSqlServer(connectionString)
            .UseLazyLoadingProxies();
        optionsBuilder.LogTo((string log) =>
        {
            StatementCounter++;
            Console.WriteLine(log);
        }, Microsoft.Extensions.Logging.LogLevel.Information);
    }
    
    public DbSet<Person> People { get; set; }
}