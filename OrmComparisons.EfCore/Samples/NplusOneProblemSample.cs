﻿using Microsoft.EntityFrameworkCore.Migrations.Operations;
using OrmComparisons.EfCore.Models;

namespace OrmComparisons.EfCore.Samples;

public class NplusOneProblemSample
{
    public static void Run()
    {
        using var context = new ApplicationContext();
        InsertData();

        context.StatementCounter = 0;
        foreach (var person in context.People.ToList())
        {
            Console.Write($"Person {person.Name} has hobbies: ");
            foreach (var hobby in person.Hobbies)
            {
                Console.Write(hobby.Name + ", ");
            }
            Console.WriteLine();
        }
        
        Console.WriteLine($"Executed {context.StatementCounter} commands");
    }

    private static void InsertData()
    {
        using var context = new ApplicationContext();
        var items = new List<Person>()
        {
            new("Jack", new List<Hobby>() { new("Running"), new("Swimming") }),
            new("Jane", new List<Hobby>() { new("Reading"), new("Gaming") }),
            new("Jina", new List<Hobby>() { new("Watching TV") })
        };
        
        context.People.AddRange(items);
        context.SaveChanges();
    }
}