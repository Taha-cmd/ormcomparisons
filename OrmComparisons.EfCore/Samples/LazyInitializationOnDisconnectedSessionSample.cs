﻿using OrmComparisons.EfCore.Models;

namespace OrmComparisons.EfCore.Samples;

public static class LazyInitializationOnDisconnectedSessionSample
{
    public static void Run()
    {
        InsertData();
        using var context = new ApplicationContext();
        var person = context.People.FirstOrDefault();
        context.Dispose();

        try
        {
            Console.WriteLine(person.Hobbies.First());
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        
    }
    
    private static void InsertData()
    {
        using var context = new ApplicationContext();
        var items = new List<Person>()
        {
            new("Jack", new List<Hobby>() { new("Running"), new("Swimming") }),
            new("Jane", new List<Hobby>() { new("Reading"), new("Gaming") }),
            new("Jina", new List<Hobby>() { new("Watching TV") })
        };
        
        context.People.AddRange(items);
        context.SaveChanges();
    }
}